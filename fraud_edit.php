<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="fraud_edit">
<script>var pageName = "fraud_edit";</script>
    <div data-role="header">
		<a id="backButton" href="" data-transition="slide" data-direction="reverse">Back Detail</a>
    	<h1>Edit Fraud Information</h1>
            <div data-role="navbar">
                <ul>
                    <li><a id="detailButton" href="#">Detail</a></li>
                    <li><a href="#" class="ui-btn-active">Edit</a></li>
                </ul>
            </div><!-- /navbar -->
		<a id="listButton" href="" data-transition="slide">List</a>
    </div>
    
	<div data-role="content">
    
        <div class="ui-field-contain"><label for="name"><strong>Topic</strong></label><input id="topic" type="text" value="topic"></div>
        <div class="ui-field-contain"><label for="name"><strong>Content</strong></label><textarea id="content" style="height:500px;">content</textarea></div>
        
        <div style="margin-bottom:2em;">
            <div >
            <!--<p style="text-align:right;"><strong>Tag</strong></p>-->
                <ul id="tagListView" data-role="listview" data-theme="a" data-inset="true" data-split-icon="minus">
                    <li><a href="index.html">#Tag<span class="ui-li-count">999</span></a></li>
                </ul>
            </div>
            <div class="ui-btn-inline"><a id="addTag" class="ui-shadow ui-btn ui-corner-all" href="/rex/fraud_tag_add.php" data-rel="dialog" data-transition="slidedown">Add Tag</a></div>
    	</div>

		<div style="margin-bottom:1em;">
			<!--<p style="text-align:right;"><strong>Tag</strong></p>-->
			<ul id="YoutubeListView" data-role="listview" data-theme="a" data-inset="true" data-split-icon="minus">
				<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child">Image</li>
				<li style="word-wrap: break-word; white-space:normal;">URL<br><input id="img" type="text" value=""></li>
			</ul>
		</div>

        <div style="margin-bottom:1em;">
            <!--<p style="text-align:right;"><strong>Tag</strong></p>-->
            <ul id="YoutubeListView" data-role="listview" data-theme="a" data-inset="true" data-split-icon="minus">
           		<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child">Map</li>
                <li style="word-wrap: break-word; white-space:normal;">Location: Hong Kong<br><input id="map" type="text" value=""></li>
            </ul>
    	</div>
        
        <div style="margin-bottom:2em;">
            <!--<p style="text-align:right;"><strong>Tag</strong></p>-->
            <ul id="YoutubeListView" data-role="listview" data-theme="a" data-inset="true" data-split-icon="minus">
           		<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child">Youtube</li>
                <li style="word-wrap: break-word; white-space:normal;">Example: https://www.youtube.com/watch?v=<font style="color:red;">5_pMx7IjYKE</font><br><input id="youtube" type="text" value=""></li>
            </ul>
    	</div>
        
        <div><input id="submitButton" type="submit" value="Submit"><div>


    
    </div>
    
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	// reading data
	var currentId = <?php echo $_GET["id"]; ?>;
	
	$("#fraud_edit #backButton").attr("href", rootPath + "/fraud/view/"+ currentId);
	$("#fraud_edit #detailButton").attr("href", rootPath + "/fraud/view/"+ currentId);
	$("#fraud_edit #listButton").attr("href", rootPath + "/fraud");
	
	// try auto login if session exist
	if(localStorage.getItem("login") == null){
		alert("You have to login for editing!");
		//window.location.replace(rootPath + "/fraud/view/" + currentId);
		window.location.replace(rootPath + "/autologin");
	}
	
	var exeJson = function(cb){
    	$.getJSON(rootPath + "/program/fraud/view/" + currentId, function(obj){
			//var items = '<li data-icon="false"><a href="#" class="ui-btn" data-transition="slide">' + obj["topic"] +':'+ obj["content"] +'</a></li>';
			cb(obj);
		});
	}
	
	function itemsCallback(obj){
		printLog(JSON.stringify(obj));
		$("#fraud_edit #topic").val(obj["topic"]);
		$("#fraud_edit #content").val(obj["content"]);
		$("#fraud_edit #img").val(obj["img"]);
		$("#fraud_edit #youtube").val(obj["youtube"]);
		$("#fraud_edit #map").val(obj["map"]);
		if(obj["isLock"] == true){
			alert('Lock');
			window.location.replace( rootPath + '/fraud');
		}
	}
	
	exeJson(itemsCallback);
	
	// submit edit data
	$("#fraud_edit #submitButton").click(function(){
		var urls = rootPath + "/program/fraud/edit";
          
		var id = currentId;
		var topic=$("#fraud_edit #topic").val();
		var content = $("#fraud_edit #content").val();
		var img = $("#fraud_edit #img").val();
		var youtube = $("#fraud_edit #youtube").val();
		var map = $("#fraud_edit #map").val();
		
		var dataPut = "id=" + id + "&topic=" + topic + "&content=" + content + "&img="+ img + "&youtube="+ youtube + "&map="+map;
		$.ajax({
			url: urls,
			data: dataPut,
			type: "PUT",
			dataType:'text',

			success: function(msg){
				printLog("edit OK, " + msg);
				var obj = JSON.parse(msg);
				if(obj["result"] == true){
					window.location.replace(rootPath + "/fraud/view/" + id);
				}else{
					alert(obj["reason"]);
				}
			},

			error:function(xhr, ajaxOptions, thrownError){
				printLog(xhr.status);
				printLog(thrownError);
			}
		});
	});
	
	// get all tag
	function tagListView(cb){
		$.getJSON( rootPath + '/program/fraud/view/'+ currentId +'/tag', function(json){
			printLog(JSON.stringify(json), "json");
			$("#fraud_edit #tagListView").html("");
			$("#fraud_edit #tagListView").html('<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child">Tag</li>');
			var datas = json['data'];
			$.each(datas, function(index, obj){
				//usage count
				var tagId = obj['id'];
				$.getJSON(rootPath + '/program/tag/'+tagId+'/usageCount', function(json){
					//$("#fraud_edit #tagListView").append('<li><a href="'+ rootPath + '/tagTopic/'+ tagId +'" data-transition="slide">#'+obj["name"]+'<span class="ui-li-count ui-body-inherit">'+json["usageCount"]+'</span></a></li>');
					$("#fraud_edit #tagListView").append('<li><input type="hidden" value="'+ tagId +'"><a href="'+ rootPath + '/tagTopic/'+ tagId +'">#' + obj["name"] + '<span class="ui-li-count ui-body-inherit">'+json["usageCount"]+'</span></a><a id="split"></a></li>');
					$("#fraud_edit #tagListView").listview('refresh');
					
					cb(datas.length);
					
				});
			});
			$("#fraud_edit #tagListView").listview('refresh');
			
			
		});
	}
	
	// add tag
	$("#fraud_edit #addTag").click(function(){
		printLog("click addTag, tempFraudId = " + currentId);
		// pass vars
		localStorage.setItem("tempFraudId", currentId);
	});
	
	//del tag
	var tagListViewCount = 0;
	function tagListViewCallback(listLength){
		if( tagListViewCount == listLength -1 ){ // already load all
			$("#fraud_edit #tagListView li #split").click(function(){
				var index = $(this).parent().index();
				printLog('index = '+$(this).parent().index() +",input id = "+$(this).parent().find('input').val());
				$.ajax({
					url:rootPath + "/program/tag/delete",
					type:"DELETE",
					data:{ deleteTagId:$(this).parent().find('input').val(), fraudId:currentId },
					success:function(msg){
						printLog(msg,'delete');
						var obj = JSON.parse(msg);
						if(obj['result'] == true){
							alert('Delete Tag Complete');
							$("#fraud_edit #tagListView li").eq(index).detach();
						}
					}
				});
			});
		}
		
		tagListViewCount++;
	}
	tagListView(tagListViewCallback);
	
	
	
});

</script>
</div>

</body>
</html>
