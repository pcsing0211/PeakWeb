<?php
@session_start();
require_once 'facebook/sdk/src/Facebook/autoload.php';
require_once 'program/common.php';
require_once 'program/database.php';

$fb = new Facebook\Facebook([
      'app_id' => '945965162167448',
      'app_secret' => '772d9ec87f6dcbf65c905b03eb56df2e',
      'default_graph_version' => 'v2.5'
]);


//peak.rurishiki.com/rex/facebook/login.php

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email', 'public_profile', 'user_friends']; // Optional permissions
$loginUrl = $helper->getLoginUrl('http://peak.rurishiki.com/rex/facebook/fb-callback.php', $permissions);

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login Facebook</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="login_facebook">
<script>var pageName = "login_facebook";</script>
	<div data-role="header">
        <a id="backButton" href="#" data-transition="slideup" data-direction="reverse">Back</a>
        <h1>Facebook</h1>
	</div>
	<div data-role="content">
        <div style="text-align:center; ">
        	<?php $userId = @$_SESSION['loginId']; ?>
        	<?php if($userId!=null){ $accessToken = Common::getDBSingleValue('member','accessToken',"where id=$userId"); ?>
           		<?php if($accessToken != ""){ ?>
                <p style="color:red;">You already conntect to facebook!</p>
                <?php }
			}?>
        	<img src="/rex/images/facebook-developers-logo.png" height="128" width="128">
        	<p><?php echo '<a href="' . htmlspecialchars($loginUrl) . '">Connect with Facebook</a>';
     ?></p>
     		
        </div>
	</div>
<?php include("footer.php"); ?>
<script>
	//$.mobile.showPageLoadingMsg();

	
$(document).ready(function(e) {
	$("#login_facebook #backButton").attr("href", rootPath + "/");

});
</script>

</div>
</body>
</html>
