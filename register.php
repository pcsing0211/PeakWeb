<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="register">
<script>var pageName = "register";</script>
	<div data-role="header">
  <a id="backButton" href="#" data-transition="pop" data-direction="reverse">Back</a>

		<h1>Register</h1>
            <a href="index.html" data-role="button">Home</a>

	</div>
	<div data-role="content">
	  <p>Email:
        <input type="text" name="register_account" id="register_account">
      </p>
      <p>Password:
        <label for="password2"></label>
        <input type="password" name="register_password" id="register_password">
      </p>
      <p>Nick Name:
        <input type="text" name="nickname" id="nickname">
      </p>
      <p id="registerResult" style="color:red"></p>
	  <p>
	    <input type="submit" name="submit" id="submit" value="Submit"  data-transition="slide">
	  </p>
  </div>
<?php include("footer.php"); ?>
<script>
$("#register #backButton").attr("href", rootPath + "/");
</script>


<script>

	
$(document).ready(function(e) {
	// [!] Can't enter this page if already loginz
	if(localStorage.getItem("login") != null){
		alert("You already login");
		window.location.replace( rootPath + "/");
	}
	
	$("#register #submit").click(function(){
		//alert("The paragraph was clicked");
		login();
	});
});

function login(){
	// vars
	var ac=$("#register #register_account").val();
	var pw=$("#register #register_password").val();
	var nickname=$("#nickname").val();

	if(ac==""){ $("#registerResult").text("Email can not empty!"); return; }
	if(pw=="" || pw == null){ $("#registerResult").text("Password can not empty!"); return; }
	if(nickname==""){ $("#registerResult").text("Nickname can not empty!"); return; }
	
	// check is valid email
	//alert("ac = " + ac);
	if(!isValidEmailAddress(ac)){
		$("#registerResult").html("Invalid email, Please check!");
		return;
	}
	
	var URLs="program/member/register";
	var myData = 'ac='+ ac + '&pw='+pw + '&nickname=' + nickname; //build a post data structure
	$.ajax({
		url: URLs,
		data: myData,
		type:"POST",
		dataType:'text',

		success: function(msg){
			// debug
			//alert(msg);
			var obj = jQuery.parseJSON(msg);
			if(obj["result"] != true){
				$("#register #registerResult").html(obj["reason"]);
				return;
			}else{
				localStorage.setItem("login", obj["login"]);
				//alert(localStorage.getItem("login"));
				//$.cookie("login",obj["login"]);
				window.location.replace( rootPath + "/membercenter");
			}
		},

		 error:function(xhr, ajaxOptions, thrownError){
			alert(xhr.status);
			alert(thrownError);
		 }
	});
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    // alert( pattern.test(emailAddress) );
    return pattern.test(emailAddress);
};
</script>

</div>
</body>
</html>
