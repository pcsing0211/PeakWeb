<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Tag</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="tag_topic">
<script>var pageName = "tag_topic";</script>
    <div data-role="header">
		<a href="#" data-transition="slide" data-direction="reverse" id="backButton">Back Tag</a>
    	<h1>Tag Relative Topic</h1>
    </div>
	<div data-role="content">
    
        <form class="ui-filterable">
            <input id="myFilter" data-type="search" placeholder="Search Topic">
        </form>
        <ul id="tagList" data-role="listview" data-filter="true" data-input="#myFilter"  data-inset="true">
            <li><a href="#">Loading...</a></li>
            <li data-icon="plus"><a href="#">Process...</a></li>
        </ul>
    </div>
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	// vars
	var tagId = <?php echo $_GET['tagId'];?>;
	// redefine url	
	$("#tag_topic #backButton").attr("href", rootPath + "/tag");

	// load tag
	$("#tag_topic #tagList").html("");
	$.get(rootPath + "/program/tagTopic/" + tagId, {}, function(msg){
		printLog(msg);
		var msgObj = JSON.parse(msg);
		$.each(msgObj['topics'], function(index, obj){
			$("#tag_topic #tagList").append('<li><a href="'+ rootPath +'/fraud/view/' + obj['id'] + '" data-transition="slide">'+obj["topic"]+'</a></li>');
		});
		
		$("#tag_topic h1").text('#' + msgObj['tagName']);
		
		$("#tag_topic #tagList").listview('refresh');
		
	});
	
});
</script>
</div>

</body>
</html>
