	<div id="guest" data-role="footer" data-position="fixed">
		<div data-role="navbar">
            <ul>
          		<li><a id="homeButton" href="#" class="ui-btn-active" data-icon="home">Home</a></li>
          		<li><a id="loginButton" href="login" data-position-to="window" data-icon="arrow-u" data-transition="slideup">Login</a></li>
           		<li><a id="registerButton" href="register" data-position-to="window" data-icon="plus registerButton" data-transition="pop">Register</a></li>
            </ul>
		</div>
	</div>
    <div id="user" data-role="footer" data-position="fixed">
    	<div data-role="navbar">
            <ul>
          		<li><a id="homeButton" href="#" class="ui-btn-active" data-icon="home">Home</a></li>
          		<li><a id="memberCenterButton" href="login" data-position-to="window" data-icon="arrow-u" data-transition="slideup">Member Center</a></li>
           		<li><a id="logoutButton" href="register" data-position-to="window" data-icon="forward" data-transition="pop">Logout</a></li>
            </ul>
		</div>
    </div>
<?php
$pathArray = explode("\\", dirname(__FILE__));
$rootPath = "/" . $pathArray[sizeof($pathArray)-1];
//echo $rootPath;
?>
<script>

// debug function
var printLogCount = 0;
var printLog = function(text, mark){
	printLogCount++;
	
	var type = "";
	if(typeof text === 'number'){
		type = "(number)";
	}else if(typeof text === 'string'){
		type = "(string)";
		if(text == "") type = "(empty string)";
	}
	if(mark == null){
		console.log(pageName + '.php(' + printLogCount + ")\t\t" + text + "\t\t" + type);
	}else{
		console.log(pageName + '.php(' + printLogCount + ")\t\t" + mark + ' = ' + text + "\t\t" + type);
	}
}
// end debug function

// welcome message
if(localStorage.getItem("login") != null){
	$("#"+pageName+" #guest").hide();
}else{
	$("#"+pageName+" #user").hide();
}

var rootPath = "/" + window.location.pathname.split("\/")[1];
//console.log("page = "+pageName);
function updateFooter(){
	$("#"+pageName+" #homeButton").attr("href", rootPath + "/");
	$("#"+pageName+" #loginButton").attr("href", rootPath + "/login");
	$("#"+pageName+" #registerButton").attr("href", rootPath + "/register");
	
	$("#"+pageName+" #memberCenterButton").attr("href", rootPath + "/membercenter");
	$("#"+pageName+" #logoutButton").attr("href", rootPath + "/logout");
	//console.log("footer update");
}

$(document).ready(function(e) {
	updateFooter();
});
</script>

