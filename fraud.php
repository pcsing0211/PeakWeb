<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="fraud">
<script>var pageName = "fraud";</script>
    <div data-role="header">
		<a href="#" data-transition="slide" data-direction="reverse" id="backButton">Back Home</a>
    	<h1>Fraud Information</h1>
		<a id="addButton" href="#" data-transition="slide" data-icon="plus" data-iconpos="right" id="fraud_addButton">Add</a>
    </div>
	<div data-role="content">
    
        <form class="ui-filterable">
            <input id="myFilter" data-type="search" placeholder="Search for names..">
        </form>
        <!--<p id="totalPage">Page: </p>-->
        <ul id="fraudlist" data-role="listview" data-filter="true" data-input="#myFilter" data-autodividers="true" data-inset="true"> 
            <li><a href="#">Loading...</a></li>
            <li data-icon="plus"><a href="#">Process...</a></li>
        </ul>
    </div>
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	// load url
	$("#fraud #backButton").attr("href", rootPath + "/");
	$("#fraud #addButton").attr("href", rootPath + "/fraud/add");
	
	// vars
	var currentPage = <?php echo (@$_GET["page"] == null)? 0 : $_GET["page"]; ?>;
	var gobalItems = "";

	// load total page
	$.getJSON(rootPath + "/program/fraud/totalpage/10", function(obj){
		var totalPage = parseInt(obj["totalPage"]);
		
		if(currentPage == 0) $("#fraud #totalPage").append('<b>[');
		$("#fraud #totalPage").append('<a href="'+ rootPath +'/fraud/all">All</a>');
		if(currentPage == 0) $("#fraud #totalPage").append(']');
		$("#fraud #totalPage").append(' ');
		
		for(var i = 0; i < obj["totalPage"] ; i++){
			var page = i + 1;
			if(currentPage == page) $("#fraud #totalPage").append('<b>[');
			$("#fraud #totalPage").append('<a href="'+ rootPath +'/fraud/'+ page +'" data-role="button" rel="external">' + page + '</a>');
			if(currentPage == page) $("#fraud #totalPage").append(']</b>');
			$("#fraud #totalPage").append(' ');
		}
	});
	
	
		
	var exeJson = function(cb){
    	$.getJSON( rootPath + "/program/fraud/topic/" + currentPage, function(obj){
			var data = obj["data"];
			var items = "";
			$.each(data, function(index, dataObj){
				//console.log(dataObj["topic"]);
				//items += dataObj["topic"];
				items += '<li data-icon="false"><a href="'+ rootPath +'/fraud/view/'+dataObj["id"]+'" class="ui-btn" data-transition="slide">' + dataObj["topic"] +'</a></li>';
			});
			cb(items);
		});
	}
	
	function itemsCallback(items){
		//console.log('items = ' + items);
		$("#fraud #fraudlist").html(items);
		$("#fraud #fraudlist").listview('refresh');
	}
	
	exeJson(itemsCallback);
	//console.log("gobalItems = " + gobalItems);
	
	// del
	/*$("#fraud #totalPage li").click(function(){
		alert("a");
		exeJson(itemsCallback);
	});*/
	
	$(window).scroll(function(){
		if($(document).height() > $(window).height())
		{
			if($(window).scrollTop() == $(document).height() - $(window).height()){
			  console.log("The Bottom");
			}
		}
	});
});
</script>
</div>

</body>
</html>
