<?php @session_start(); ?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="index">
<script>var pageName = "index";</script>
  <div data-role="header">
  <a href="#mypanel">Readme</a>
    <h1>Fraud App</h1>
  </div>
	<div data-role="content">
<!--        <div id="ui-bar-test" class="ui-bar ui-bar-a ui-corner-all" style="margin-bottom:2em;">
			<p>I am a div with classes ui-bar, ui-bar-<span class="theme">a</span> and ui-corner-all. <a href="#" class="ui-link">I am a link</a></p>
		</div>-->
        <div id="ui-body-test" class="ui-body ui-body-a ui-corner-all" style="text-align:center; margin-bottom:3em;">
            <p id="guestWelcomeMessage" style="padding-left:10px;">Welcome, Guest</p>
			<p id="userWelcomeMessage" style="padding-left:10px;">Welcome, User</p>
        	<?php if( @$_SESSION['loginId'] != null){ ?>
			<img src="http://peak.rurishiki.com/rex/facebook/image.php?userId=<?php echo $_SESSION['loginId']; ?>" class="ui-corner-all" style="margin-top:6px; margin-bottom:18px;">
            <?php } ?>

		</div>
       <div>
		<ul data-role="listview" style="margin-bottom:2em;">
            <li><a href="fraud" data-transition="slide">Information</a></li>
			<li><a href="tag" data-transition="slide">Tag</a></li>
			<li><a href="forum" data-transition="slide">Forum</a></li>
            <li><a href="game" data-transition="slide" rel="external">Game</a></li>
            <li><a href="ranking" data-transition="slide">Ranking</a></li>
        </ul>
        <ul data-role="listview" style="margin-bottom:2em;">
        	<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit">Member</li>
			<li><a href="membercenter" data-transition="slide">Member Center</a></li>
			<li><a href="bookmark" data-transition="slide">Bookmark</a></li>
			<li><a href="login/facebook" rel="external">Facebook</a></li>
        </ul>
	    <ul data-role="listview" style="margin-bottom:1em;">
        	<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit">Developer</li>
            <li><a href="api" rel="external">API</a></li>
		</ul>
        </div>
        	<p style="text-align:center;">2016 Web Assignment - Poon Chun Sing</p>

	</div>
  


<?php include("footer.php"); ?>
    <div id="mypanel" data-role="panel">
        <div class="ui-panel-inner">
          <h3>Readme</h3>
        <p>This is a web app about fraud</p>
        <p>You can know more fraud information from here</p>
        <a href="#demo-links" data-rel="close" class="ui-btn ui-shadow ui-corner-all ui-icon-delete ui-btn-icon-left ui-btn-inline">Close panel</a></div>
    </div>
<?php
if(@$_SESSION["loginId"]==null){
?>
<script>
	if(localStorage.getItem("login") != null){
		
		// try login
		window.location.replace( rootPath + "/autologin");
		//alert("auto login");
	}
</script>
<?php
}else{
	$loginId = @$_SESSION["loginId"];
	echo "<script>printLog('$loginId', 'loginId');printLog(localStorage.getItem('login'));</script>";
}
?>
<script>
$(document).ready(function(e) {
	// [!] Can't enter this page if already loginz
	if(localStorage.getItem("login") != null){
		// already login
		$("#index #guestWelcomeMessage").hide();
		$("#index #userWelcomeMessage").text("Welcome, " + localStorage.getItem("login").split(":")[2]);
	}else{
		$("#index #userWelcomeMessage").hide();
	}
});
</script>
    <div data-role="popup" id="popupLogin" data-theme="a" class="ui-corner-all">
        <form>
            <div style="padding:10px 20px;">
              <h3>Please sign in</h3>
              <label for="un" class="ui-hidden-accessible">Username:</label>
              <input type="text" name="user" id="un" value="" placeholder="username" data-theme="a" />

              <label for="pw" class="ui-hidden-accessible">Password:</label>
              <input type="password" name="pass" id="pw" value="" placeholder="password" data-theme="a" />

              <button type="submit" data-theme="b">Sign in</button>
            </div>
        </form>
   </div>
</div>

</body>
</html>
