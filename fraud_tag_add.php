<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Member Center</title>
<link href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js" type="text/javascript"></script>

</head>

<body>
<div data-role="dialog" id="fraud_add_tag">
	<div data-role="header">
		<a href="index.html" data-rel="back" data-transition="slide" data-direction="reverse">Back</a>
        <h1>Add Tag</h1>
	</div>
	<div data-role="content">
        <input id="text" type="text" value="">
        <input id="submit" type="submit" value="Submit">
	</div>
<script>
$("#fraud_add_tag #submit").click(function(){
	var currentId = 0;
	var fraudId = localStorage.getItem("tempFraudId");	// get passed vars
	var newTagText = $("#fraud_add_tag #text").val();
	
	// can't empty
	if(newTagText == ""){
		alert("Tag can't not empty");
		return;
	}
	
	// update current id
	currentId = fraudId;
	printLog(currentId, 'fraudId');
	
	// post
	$.post( rootPath + '/program/tag/add', {topicId:currentId, newTag:newTagText} , function(msg){
		printLog(msg,'tagAdd');
		var obj = JSON.parse(msg);
		if(obj['result'] == false){
			alert('Tag already added to this fraud');
			return;	
		}
		
		// ok
		if(fraudId != null){
			currentId = fraudId;
			localStorage.removeItem("tempFraudId");
		}else{
			alert('Error, can\'t get tempFraudId');
			return;
		}
		//window.location.replace(rootPath+'/fraud/view/'+currentId);
		$('[data-role=dialog]').dialog("close");
		//$("#fraud_edit #tagListView").append('<li><input type="hidden" value="'+ tagId +'"><a href="'+ rootPath + '/tagTopic/'+ tagId +'">#' + obj["name"] + '<span class="ui-li-count ui-body-inherit">'+json["usageCount"]+'</span></a><a id="split"></a></li>');
		$('#fraud_edit #tagListView').append('<li><a href="'+rootPath+'/tagTopic/'+obj['addTagId']+'" data-transition="slide">#'+newTagText+'<span class="ui-li-count ui-body-inherit">'+(obj['usageCount']+1)+'</span></a><a id="split"></a></li>');
		$('#fraud_edit #tagListView').listview('refresh');
	});
	
});
</script>
</div>
</body>
</html>
