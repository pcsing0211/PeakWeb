<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="change_password">
<script>var pageName = "change_password";</script>
	<div data-role="header">
  <a id="backButton" href="#" data-transition="pop" data-direction="reverse">Back</a>

		<h1>Change Password</h1>
            <a href="index.html" data-role="button">Home</a>

	</div>
	<div data-role="content">
	  <p>Old Password:
        <input type="password" id="old_password">
      </p>
      <p>New Password:
        <input type="password" id="new_password">
      </p>
      <p>Confirm New Password:
        <input type="password" id="new_pasword_confirm">
      </p>
      <p id="changeResult" style="color:red"></p>
	  <p>
	    <input type="submit" name="submit" id="submit" value="Submit"  data-transition="slide">
	  </p>
  </div>
<?php include("footer.php"); ?>
<script>
$("#register #backButton").attr("href", rootPath + "/");
</script>


<script>
$(document).ready(function(e) {
	$("#change_password #backButton").attr("href", rootPath + "/");

	// try to login firstly
	if(localStorage.getItem("login") == null){
		window.location.replace( rootPath + "/autologin");
	}
	
	$("#change_password #submit").click(function(){
		// vars
		var oldPassword=$("#change_password #old_password").val();
		var newPassword=$("#change_password #new_password").val();
		var newPasswordConfirm=$("#change_password #new_pasword_confirm").val();
	
		// can't empty
		if(oldPassword==""){ $("#changeResult").text("Old Password can not empty!"); return; }
		if(newPassword==""){ $("#changeResult").text("New Password can not empty!"); return; }
		if(newPasswordConfirm==""){ $("#changeResult").text("New Password Confirm can not empty!"); return; }
		
		// new password confirm failure
		if(newPassword != newPasswordConfirm){ $("#changeResult").text("New Password is not same as confirm"); return; }
		
		// new password can't same as old one
		if(newPassword == oldPassword){ $("#changeResult").text("New Password can't same as old one"); return; }
		
		// change password
		$.ajax({
			url: rootPath + '/program/member/changePassword',
			type: 'PUT',
			data: {oldPassword: oldPassword, newPassword: newPassword},
			success: function(msg) {
				printLog(msg);
				var obj = JSON.parse(msg);
				
				if(obj['result'] == false && obj['reason']=='old-password-wrong'){
					$("#changeResult").text("Old password wrong!"); return;
				}
				if(obj['result'] == true){
					alert("Password change complete!");
					window.location.replace( rootPath + '/membercenter');
				}
			}
		});
		/*$.post( rootPath + '/program/member/changePassword', {oldPassword:oldPassword, newPassword:newPassword}, function(msg){
	
		});*/
	});
	
	
});
</script>

</div>
</body>
</html>
