<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Forum</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="forum">
<script>var pageName = "fraud";</script>
    <div data-role="header">
		<a href="#" data-transition="slide" data-direction="reverse" id="backButton">Back Home</a>
    	<h1>Forum</h1>
		<a id="addButton" href="#" data-transition="slide" data-icon="plus" data-iconpos="right" id="fraud_addButton">Add</a>
    </div>
	<div data-role="content">
    
        <form class="ui-filterable">
            <input id="myFilter" data-type="search" placeholder="Search for names..">
        </form>
        <!--<p id="totalPage">Page: </p>-->
        <ul id="forumListView" data-role="listview" data-filter="true" data-input="#myFilter" data-inset="true"> 
            <li><a href="#">Loading...</a></li>
            <li data-icon="plus"><a href="#">Process...</a></li>
        </ul>
    </div>
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	// load url
	$("#forum #backButton").attr("href", rootPath + "/");
	$("#forum #addButton").attr("href", rootPath + "/forum/add");

	// load total page
	$.getJSON(rootPath + "/program/forum/allTopic", function(obj){
		$("#forum #forumListView").html("");
		
		printLog(JSON.stringify(obj));
		$.each(obj['data'], function(index, itemObj){
			printLog(itemObj['topic']);
			$("#forum #forumListView").append('<li><a href="'+ rootPath +'/forum/view/'+ itemObj['id'] +'" data-role="button" rel="external">' + itemObj['topic'] + '</a></li>');

		});
		
		$("#forum #forumListView").listview('refresh');
	});

});
</script>
</div>

</body>
</html>
