<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ranking</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="ranking">
<script>var pageName = "ranking";</script>
    <div data-role="header">
		<a href="#" data-transition="slide" data-direction="reverse" id="backButton">Back Home</a>
    	<h1>Ranking</h1>
    </div>
	<div data-role="content">
        <!--<p id="totalPage">Page: </p>-->
        <ul id="rankingListView" data-role="listview" data-filter="true" data-input="#myFilter" data-inset="true">
            <li><a href="#">Loading...</a></li>
            <li data-icon="plus"><a href="#">Process...</a></li>
        </ul>
    </div>
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	// load url
	$("#ranking #backButton").attr("href", rootPath + "/");

	// load total page
	$.getJSON(rootPath + "/program/game/ranking", function(obj){
		$("#ranking #rankingListView").html("");
		
		printLog(JSON.stringify(obj));
		$.each(obj['data'], function(index, itemObj){
			$("#ranking #rankingListView").append('<li><b>('+(index+1)+')</b>\t  ' + itemObj['player'] +'<span class="ui-li-count ui-body-inherit">'+itemObj['score'] + '</span></li>');

		});
		
		$("#ranking #rankingListView").listview('refresh');
	});

});
</script>
</div>

</body>
</html>
