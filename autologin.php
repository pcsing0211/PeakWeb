<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="autoLogin">
<script>var pageName = "autologin";</script>
  <div data-role="header">
    <h1>Auto Login</h1>
  </div>
	<div data-role="content"><p id="result">Connecting to Login Process...</p>
	</div>
  
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	//alert("footer");
	// [!] Can't enter this page if already loginz
	if(localStorage.getItem("login") != null){
		//alert("footer login");
		var loginData = localStorage.getItem("login").split(":");
		//alert(loginData[0]);
		$.post( rootPath + "/program/member/autologin", {ac:loginData[0], pw:loginData[1]}, function(msg){
			//alert(msg);
			var obj = JSON.parse(msg);
			if(obj["result"] == true){
				$("#autoLogin #result").text("Session Fround! Login Complete! Back to Home after 3 second ...");
				setTimeout(function(){ window.location.replace( rootPath + "/"); }, 3000);
			}else{
				$("#autoLogin #result").text("Account not Fround! Please register ...");
				setTimeout(function(){ window.location.replace( rootPath + "/register"); }, 3000);
			}
			
			//alert("obj = "+JSON.stringify(obj));
			
			// auto login complete
			//window.location.replace( rootPath + "/");
			//window.history.back();
		});
	}else{
		$("#autoLogin #result").text("Session not Fround! Redirect to login page ...");
		window.location.replace( rootPath + "/login");
	}
});
</script>
</div>

</body>
</html>
