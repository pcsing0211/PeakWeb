<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Member Center</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="membercenter">
<script>var pageName = "membercenter";</script>


	<div data-role="header">
		<a id="backButton" href="#" data-transition="slideup" data-direction="reverse">Back Home</a>
		<h1>Member Center</h1>
	</div>
	<div data-role="content">
	<div id="panel_control">
		<p><span id="nickname"></span></p>
        <p><span id="ac"></span></p>
       	<div class="ui-block-a"><a id="editNickname" class="ui-shadow ui-btn ui-corner-all" href="/rex/membercenter/editNickname" data-rel="dialog" data-transition="slidedown">Edit Nickname</a></div>
        <div class="ui-block-a"><a class="ui-shadow ui-btn ui-corner-all" id="chnagePassword" href="chnagePassword">Change Password</a></div>
        <div class="ui-block-a"><a class="ui-shadow ui-btn ui-corner-all" id="deleteAccount" href="#">Delete Account</a></div>
        <div class="ui-block-a"><a href="logout"  class="ui-shadow ui-btn ui-corner-all" id="logout" href="logout">Logout</a></div>
    </div>
    
    <div id="panel_guest">
    	<p style="text-align:center;">You havn't login >0<</p>
    </div>
  </div>
<?php include("footer.php"); ?>
<?php
session_start();
if($_SESSION["loginId"] != null){
	$loginId = $_SESSION["loginId"];
	echo "<script>printLog('$loginId','loginId');</script>";
}
?>

<script>
$( document ).ready(function() {
	$("#membercenter #backButton").attr("href", rootPath + "/");
	$("#membercenter #chnagePassword").attr("href", rootPath + "/membercenter/changePassword");
	
//alert(localStorage.getItem("login"));
	var login = localStorage.getItem("login");
	if(login != null){
		$("#membercenter #panel_guest").hide();
		var ac = login.split(":")[0];
		var nickname = login.split(":")[2];
		$("#ac").html("<strong>Account</strong>: " + ac);
		$("#nickname").html("<strong>Nickname</strong>: " + nickname);
	}else{
		$("#membercenter #panel_control").hide();
	}
	
	/*$('#membercenter #editNickname').click(function(){
		localStorage.setItem('', '');
	});*/
	
	$("#membercenter #deleteAccount").click(function(){
		$.ajax({
			url: rootPath + '/program/member/deleteAccount',
			type: 'DELETE',
			success: function(msg) {
				printLog(msg);
				var obj = JSON.parse(msg);
				
				if(obj['result'] == true){
					alert("Account delete complete!");
					localStorage.removeItem("login");
					window.location.replace( rootPath + '/');
				}
			}
		});
		
	});
});
</script>

</div>
</body>
</html>
