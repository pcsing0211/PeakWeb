<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Member Center</title>
<link href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js" type="text/javascript"></script>

</head>

<body>
<div data-role="page" id="membercenter">



  <div data-role="header">
  <a href="index.html" data-transition="slide" data-direction="reverse">Back</a>
    <h1>Header</h1>
  </div>
  <div data-role="content"> </div>
<?php include("footer.php"); ?>
  
  
	<script>
	$(document).ready(function(e) {
		$("#popupBasic").popup("open");
		//alert(localStorage.getItem("login"));
		var login = localStorage.getItem("login");
		if(login != null){
			localStorage.removeItem("login");
			$.get( rootPath + '/program/member/logout', {}, function(msg){
				printLog(msg, "get result");
			});
			printLog("Logout Success");
			
		}else{
			printLog("Error! Can not find login record.");
		}
		
		window.location.replace( rootPath + "/");
		
		//setTimeout(function(){alert("Hello");},4000);
	});
    </script>
    
    
</div>
</body>
</html>
