<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="forum_add">
<script>var pageName = "forum_add";</script>
    <div data-role="header">
		<a id="backButton" href="" data-transition="slide" data-direction="reverse">Back Detail</a>
    	<h1>Add Fraud Information</h1>
    </div>
    
	<div data-role="content">
    
        <p><input id="topic" type="text" value="New topic"></p>
        <p><textarea id="content">Content</textarea></p>
        <p><input id="submitButton" type="submit" value="Submit"><p>
    </div>
    
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	// reading data
	var gobalItems = "";
	
	// load url
	$("#forum_add #backButton").attr("href", rootPath + "/forum");
	
	// check is login already
	if(localStorage.getItem("login") == null){
		alert("You have to login for adding!");
		window.location.replace( rootPath + "/autologin");
	}
	
	// submit edit data
	$("#forum_add #submitButton").click(function(){
		printLog("click");
		var urls = rootPath + "/program/forum/add";

		var topic=$("#forum_add #topic").val();
		var content = $("#forum_add #content").val();
		
		var dataPut = "topic=" + topic + "&content=" + content;
		$.ajax({
			url: urls,
			data: dataPut,
			type: "POST",
			dataType:'text',

			success: function(msg){
				//alert("add OK, " + msg);
				var obj = JSON.parse(msg);
				if(obj["result"] == true){
					window.location.replace(rootPath + "/forum");
					printLog('OK');
				}else{
					alert(obj["reason"]);
				}
			},

			error:function(xhr, ajaxOptions, thrownError){
				alert(xhr.status);
				alert(thrownError);
			}
		});
	});
});
</script>
</div>

</body>
</html>
