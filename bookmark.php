<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bookmark</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="bookmark">
<script>var pageName = "bookmark";</script>
    <div data-role="header">
		<a href="#" data-transition="slide" data-direction="reverse" id="backButton">Back</a>
    	<h1>Bookmark List</h1>
        <input id="mode" type="hidden" value="view">
        <a id="editButton" href="">Edit</a>
    </div>
    
	<div data-role="content">
    
        <form class="ui-filterable">
            <input id="myFilter" data-type="search" placeholder="Search for names..">
        </form>
        <ul id="bookmarkList" data-role="listview" data-filter="true" data-input="#myFilter" data-inset="true"> 
            <li><a href="#">Loading...</a></li>
            <li data-icon="plus"><a href="#">Process...</a></li>
        </ul>
        <p id="bookmarkEditAlert" style="text-align:center; color:red; "><strong>You are now on editing mode</strong> ...</p>
        <ul id="bookmarkEditList" data-role="listview" data-filter="true" data-input="#myFilter" data-inset="true" data-split-icon="minus"> 
          <li><a href="#">Loading...</a><a href="del"></a></li>
            <li><a href="#">Process...</a><a href="del"></a></li>
        </ul>
    </div>
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	if(localStorage.getItem("login") == null){
		window.location.replace( rootPath + '/autologin' );	
		return;
	}
	
	// load url
	$("#bookmark #backButton").attr("href", rootPath + "/");
	
	// vars
	var currentPage = <?php echo (@$_GET["page"] == null)? 1 : $_GET["page"]; ?>;

	// get json
	var urlJson = function(u, cb){
    	$.getJSON( rootPath + "/program/member/allBookmark", function(obj){
			var bookmarks = obj["bookmarks"].split(",");
			printLog(bookmarks);
			$.each(bookmarks, function(index, value){
				//var i = 0;
				var jsonAjax = $.getJSON( rootPath + "/program/fraud/getTopic/" + value, function(bookmarkObj){
					var id = bookmarkObj["id"];
					var topic = bookmarkObj["topic"];
					$("#bookmark #bookmarkList").append('<li><a href="'+ rootPath +'/fraud/view/'+ id+'" data-transition="slide">' + topic + '</a></li>');
					$("#bookmark #bookmarkEditList").append('<li><input type="hidden" value="'+ id +'"><a>' + topic + '</a><a class="split"></a></li>');
					cb(bookmarks.length, index);
					//i++;
	
					//if(i == (bookmarks.length -1)){ alert(i); $("#fraud #bookmarkList").listview('refresh'); }
				});
				//jsonAjax.complete(function(){ alert("jsonAjax  c ");});
			});
			u();
		});
	}
	
	// [rex] callback function can directly get gobal vars
	var itemsCount = 0;
	var newMode = "view";
	function itemsCallback(length, index){
		itemsCount++;
		
		if(itemsCount == length){
			//console.log("(bookmark.php) All loading Complete!");
			
			// refresh on complete
			$("#bookmark #bookmarkList").listview('refresh');
			$("#bookmark #bookmarkEditList").listview('refresh');
			
			// connect click event on ajax complete
			$("#bookmark #bookmarkEditList li #split").click(function(){
				var index = $(this).index();
				var id = $(this).parent().find("input").val();
				// post
				$.post( rootPath + "/program/bookmark/del", {topicId:id}, function(msg){
					printLog( "msg = " + msg );
					var obj = JSON.parse(msg);
					if(obj["result"] == true){
						window.location.replace( rootPath + "/bookmark" );
						//pcb();
					}
				}); 
				printLog( "id = " + id );
			});
			
			// refuse on init
			switchList();
		}
		
	}

	function postCallback(){
		alert("postCallback");
		urlJson(updateItems, itemsCallback, postCallback);
		$("#bookmark #bookmarkList").listview('refresh');
		$("#bookmark #bookmarkEditList").listview('refresh');
	}
	
	function updateItems(){
		// refresh on first
		$("#bookmark #bookmarkList").listview('refresh');
		$("#bookmark #bookmarkEditList").listview('refresh');
		//alert("u");
	}
	
	// clear at first
	$("#bookmark #bookmarkList").html("");
	$("#bookmark #bookmarkEditList").html("");
	urlJson(updateItems, itemsCallback, postCallback);
	
	/*$(window).scroll(function(){
		if($(document).height() > $(window).height())
		{
			if($(window).scrollTop() == $(document).height() - $(window).height()){
			  console.log("The Bottom");
			}
		}
	});*/
	
	// saved data
	if(localStorage.getItem("bookmarkMode") != null){
		newMode = localStorage.getItem("bookmarkMode");
		$("#bookmark #mode").val( newMode );
		//alert( newMode );
		switchList();
	}
	
	// flag of edit and view
	$("#bookmark #editButton").click(function(){
		newMode = $("#bookmark #mode").val() == "edit" ? "view" : "edit";
		localStorage.setItem("bookmarkMode", newMode);
		$("#bookmark #mode").val( newMode );
		
		switchList();
	});
	
	// swtich content list and button word
	function switchList(){
		if(newMode == "view"){
			$("#bookmark #editButton").text( "Edit" );
			$("#bookmark #bookmarkEditList").hide();
			$("#bookmark #bookmarkEditAlert").hide();
			$("#bookmark #bookmarkList").show();
		}else{
			$("#bookmark #editButton").text( "View" );
			$("#bookmark #bookmarkEditList").show();
			$("#bookmark #bookmarkEditAlert").show();
			$("#bookmark #bookmarkList").hide();
		}
	}
});
</script>
</div>

</body>
</html>
