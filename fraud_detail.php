<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fraud</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="fraud_detail" data-add-back-btn="true">
<script>var pageName = "fraud_detail";</script>
    <div data-role="header">
		<a id="backButton" href="" data-transition="slide" data-direction="reverse">Back</a>
   	    <h1>Fraud Information</h1>
            <div data-role="navbar">
                <ul>
                    <li><a href="#" class="ui-btn-active">Detail</a></li>
                    <li><a id="editButton" href="#">Edit</a></li>
                </ul>
            </div><!-- /navbar -->
    <a id="bookmarkButton" href="#">Bookmark</a>
    </div>
    
	<div data-role="content">
        <h2 id="topic"></h2>

        <div id="ui-body-test" class="ui-body ui-body-a ui-corner-all" style="margin-bottom:2em;">
        	<p id="content"></p>
		</div>
        <!--<p id="tag"><strong>Tag:</strong> <a>#iphone</a> <a>#ios</a> <a>#android</a></p>-->
        <!--<p id="timestamp" style="text-align:right;"><strong>(Last Edit: 2000-0-0)</strong></p>-->

		<div id="ui-body-test" class="ui-body ui-body-a ui-corner-all" style="margin-bottom:2em;">
			<img id="img" src="" width="100%" height="300px"></li>
		</div>

		<div style="margin-bottom:2em;">
            <!--<p style="text-align:right;"><strong>Tag</strong></p>-->
            <ul id="MaptubeListView" data-role="listview" data-theme="a" data-inset="true" data-split-icon="minus" class="ui-listview ui-listview-inset ui-corner-all ui-shadow ui-group-theme-a">
           		<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-bar-a ui-first-child">Map</li>
                <li style="word-wrap: break-word; white-space:normal;" class="ui-li-static ui-body-inherit ui-last-child"><div id="map" style="margin-bottom:1em;"></div><div id="map_canvas" style="height:200px"></div></li>
            </ul>
    	</div>
        
        <div style="margin-bottom:1em;">
            <!--<p style="text-align:right;"><strong>Tag</strong></p>-->
            <ul id="YoutubeListView" data-role="listview" data-theme="a" data-inset="true" data-split-icon="minus" class="ui-listview ui-listview-inset ui-corner-all ui-shadow ui-group-theme-a">
           		<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-bar-a ui-first-child">Youtube</li>
                <li id="youtube" style="word-wrap: break-word; white-space:normal;" class="ui-li-static ui-body-inherit ui-last-child"></li>
            </ul>
    	</div>
        
        <div style="margin-bottom:2em;">
            <div >
            <!--<p style="text-align:right;"><strong>Tag</strong></p>-->
                <ul id="tagListView" data-role="listview" data-theme="a" data-inset="true">
                    <li><a href="index.html">#Tag<span class="ui-li-count">999</span></a></li>
                </ul>
            </div>
    	</div>
        <div data-theme="a" data-content-theme="a">
            <div >
                <ul id="authorListView" data-role="listview" data-filter="true" data-filter-theme="a" data-divider-theme="b" data-inset="true">
                    <li><a href="index.html">Author1</a></li>
                    <li><a href="index.html">Author2</a></li>
                </ul>
            </div>
        </div>
              
    </div>
    
    <div data-role="footer" data-position="fixed">
      <div data-role="navbar">
          <ul>
            <li><a href="#" class="ui-btn-active homeButton" data-icon="home">Home</a></li>
            <li><a href="#popupLogin" data-rel="popup" data-position-to="window" data-icon="arrow-u">Login</a></li>
            <li><a href="#popupLogin" data-rel="popup" data-position-to="window" data-icon="plus">Register</a></li>
          </ul>
      </div>
	</div>
<?php include("footer.php"); ?>
<script>
var mlat = 0;
var mlng = 0;
	
$(document).ready(function(e) {
	var currentId = <?php echo $_GET["id"]; ?>;

	$("#fraud_detail #backButton").attr("href", rootPath + "/fraud");
	$("#fraud_detail #editButton").attr("href", rootPath + "/fraud/view/"+ currentId +"/edit");


	// update size

	var resizeObj = function(){
		$("#fraud_detail #youtube iframe").attr('height',$(window).width()* 0.6);
		$("#fraud_detail #img").attr('height',$(window).width()* 0.6);

	}
	$( window ).resize(function() {
		resizeObj();
//		if($("#fraud_detail #map_canvas").html() !=""){
//			$("#fraud_detail #map_canvas").css('height',$(window).width()* 0.5);
//		}else{
//			$("#fraud_detail #map_canvas").css('height',0);
//		}
	})
	resizeObj();
	var exeJson = function(cb){
    	$.getJSON(rootPath + "/program/fraud/view/" + currentId, function(obj){
			//var items = '<li data-icon="false"><a href="#" class="ui-btn" data-transition="slide">' + obj["topic"] +':'+ obj["content"] +'</a></li>';
			cb(obj);
			loadMap(obj, loadMapCB);
		});
	}
	
	function updateItemsCallback(obj){
		printLog(JSON.stringify(obj), "updateItemsCallback");
		
		// prevent enter null topic
		if(obj["topic"] == null){
			printLog("Topic do not exist!");
			window.location.replace( rootPath + "/");
		}
		
		var browserHeight = $(window).width()* 0.5;
		// load data
		$("#fraud_detail #topic").html(obj["topic"]);
		$("#fraud_detail #content").html(obj["content"]);
		if(obj['img'] !="") $("#fraud_detail #img").attr('src',obj['img']);
		if(obj["youtube"]!="") $("#fraud_detail #youtube").html('<iframe width="100%" height="'+browserHeight+'" src="https://www.youtube.com/embed/'+obj["youtube"]+'" frameborder="0" allowfullscreen></iframe>');
		if(obj["map"]!=""){
			$("#fraud_detail #map").html(obj["map"]);
		}else{
			$("#fraud_detail #map_canvas").html("");
		}
		
	}
	
	function loadMap(obj, cb){
		if(obj['map'] == "" ) return;
		$.get("https://maps.googleapis.com/maps/api/geocode/json?address="+obj['map']+"&key=AIzaSyAGRdEsarn8gEMBcyYwk2E_sQQfLTlYS_k",{},function(mapObj){
			printLog(JSON.stringify(mapObj),'map');
			printLog(JSON.stringify(mapObj['results'][0]['geometry']),'geometry');
			mlat = mapObj['results'][0]['geometry']['location']['lat'];
			mlng = mapObj['results'][0]['geometry']['location']['lng'];
			cb();
		});
	}
	
	// rex: Only Callback can call gobal function if 1 scope async
	// rex: Only Callback can call gobal vars and function if nest async
	var loadMapCB = function(){
		loadScript('http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=initialize', function(){});
	}
	//
	
	exeJson(updateItemsCallback,loadMap);
	
	var isBookmarked = false;
	// bookmark it
	$("#fraud_detail #bookmarkButton").click(function(){
		$.post( rootPath + '/program/bookmark', {bookmark: currentId}, function(msg){
			//alert("a");
			printLog(msg);
			
			var obj = JSON.parse(msg);
			if(obj["result"]){
				printLog("Bookmark Success!");
				window.location.replace( rootPath + '/fraud/view/' + currentId);
			}else{
				//alert(obj["reason"] + ", Unbookmark?");
				if(obj['try']){
					$.post( rootPath + '/program/bookmark/del', {topicId: currentId}, function(msg){
						var obj = JSON.parse(msg);
						if(obj['result']){
							printLog("Delete Bookmark Complete!");
							window.location.replace( rootPath + '/fraud/view/' + currentId);
						}else{
							printLog("unknown error!");
						}
					});
				}else{
					//try login
					window.location.replace( rootPath + "/autologin");
					
					printLog("try action not exist!");	
				}
				
			}
		});
	});
	
	// is bookmarked?
	$.get( rootPath + "/program/bookmark/status/" + currentId, {}, function(msg){
		printLog(msg);
		var obj = JSON.parse(msg);
		
		isBookmarked = obj["status"];
		if(obj["status"]){
			$("#fraud_detail #bookmarkButton").text("Bookmarked");
			$("#fraud_detail #bookmarkButton").addClass('ui-btn-active');
		}
	});
	
	// get all tag
	$.getJSON( rootPath + '/program/fraud/view/'+ currentId +'/tag', function(json){
		printLog(currentId, "cureentId");
		printLog(JSON.stringify(json), "tag");
		$("#fraud_detail #tagListView").html("");
		$("#fraud_detail #tagListView").html('<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child">Tag</li>');
		$.each(json['data'], function(index, obj){
			//usage count
			var tagId = obj['id'];
			$.getJSON(rootPath + '/program/tag/'+tagId+'/usageCount', function(json){
				$("#fraud_detail #tagListView").append('<li><a href="'+ rootPath + '/tagTopic/'+ tagId +'" data-transition="slide">#'+obj["name"]+'<span class="ui-li-count ui-body-inherit">'+json["usageCount"]+'</span></a></li>');
				$("#fraud_detail #tagListView").listview('refresh');
			});
		});
		$("#fraud_detail #tagListView").listview('refresh');
	});
	
	// get all author of this fraud item
	$.getJSON( rootPath + '/program/fraud/view/'+ currentId +'/author', function(json){
		$("#fraud_detail #authorListView").html("");
		$("#fraud_detail #authorListView").html('<li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child">Author</li>');
		if(json['result'] == true){
			$.each(json['data'], function(index, value){
				$("#fraud_detail #authorListView").append('<li>'+value["authorName"]+'</li>');
			});
		}
		$("#fraud_detail #authorListView").listview('refresh');
	});
	
	
	
	
});


</script>
<script>
	function loadScript(src,callback){
		var script = document.createElement("script");
		script.type = "text/javascript";
		if(callback) script.onload = callback;
		document.getElementsByTagName("head")[0].appendChild(script);
		script.src = src;
	}
function initialize() {
		printLog('load......');
			printLog(mlat, 'mlat');
		  var myLatLng = {lat: mlat, lng: mlng};
		
		  var map = new google.maps.Map($('#fraud_detail #map_canvas')[0], {
			zoom: 16,
			center: myLatLng
		  });
		
		  var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Hello World!'
		  });
	}
</script>
</div>

</body>
</html>
