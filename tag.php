<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Tag</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="tag">
<script>var pageName = "tag";</script>
    <div data-role="header">
		<a href="#" data-transition="slide" data-direction="reverse" id="backButton">Back Home</a>
    	<h1>Tag List</h1>
    </div>
	<div data-role="content">
    
        <form class="ui-filterable">
            <input id="myFilter" data-type="search" placeholder="Search Tag">
        </form>
        <ul id="tagList" data-role="listview" data-filter="true" data-input="#tag #myFilter"  data-inset="true">
            <li><a href="#">Loading...</a></li>
            <li data-icon="plus"><a href="#">Process...</a></li>
        </ul>
    </div>
<?php include("footer.php"); ?>
<script>
$(document).ready(function(e) {
	// redefine url	
	$("#tag #backButton").attr("href", rootPath + "/");

	// load tag
	$("#tag #tagList").html("");
	
	var readAllTag = function(cb){
		$.get(rootPath + "/program/tag", {}, function(msg){
			printLog(msg);
			var arr = JSON.parse(msg);
			$.each(arr, function(index, obj){
				//usage count
				var tagId = obj['id'];
				$.getJSON(rootPath + '/program/tag/'+tagId+'/usageCount', function(json){
					$("#tag #tagList").append('<li><a href="'+ rootPath + '/tagTopic/'+ tagId +'" data-transition="slide">#'+obj["name"]+'<span class="ui-li-count ui-body-inherit">'+json["usageCount"]+'</span></a></li>');
					
					//cb(arr.length);
					$("#tag #tagList").listview('refresh');
				});
				
			});
			
			
			//cb(arr.length-1, arr.length);
		});
	}
	
	var readAllTagCount = 0;
	var readAllTagCB = function(mainLength){
		if(readAllTagCount == (mainLength-1)){
			printLog('call readAllTagCB');
			printLog((mainLength-1), 'mainLength');
			
			$("#tag #tagList").listview('refresh');
		}
		readAllTagCount++;
	}
	readAllTag(readAllTagCB);
	
	
	
});
</script>
</div>

</body>
</html>
