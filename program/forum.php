<?php
include "common.php";
include "database.php";

class forum{
	public function allTopic(){	
		$sqlResult = mysql_query("select * from forum order by timestamp desc");
		while($row = mysql_fetch_assoc($sqlResult)){
			$tempObj = array();
			$tempObj['id'] = $row['id'];
			$tempObj['topic'] = $row['topic'];
			$tempObj['content'] = $row['content'];
			$json['data'][] = $tempObj;
		}
		
		Common::echoJSON(true, $json);
	}
	
	public function add(){
		$topic = $_POST['topic'];
		$content = $_POST['content'];
		$userId = @$_SESSION['loginId'];
		
		//check ac api
		if(Common::checkLoginApi(@$_POST['ac'], @$_POST['pw']) == false)
			return;
			
		mysql_query("insert into forum (topic, content, user_id) values ('$topic', '$content', $userId)");
		Common::echoJSON(true, null);
	}
	
	public function view(){
		$topicId = $_GET["topicId"];
		$sqlResult = mysql_query("select topic, content, user_id, timestamp from forum where id = $topicId");
		//$json['sql'] = "select topic, content, user_id, timestamp from forum where id = $topicId";
		if(mysql_num_rows($sqlResult) <1){
			Common::echoJSON(false, null);
			return;	
		}
		while($row = mysql_fetch_assoc($sqlResult)){
			$json['topic'] = $row['topic'];
			$json['content'] = $row['content'];
			$json['nickName'] = Common::getNicknameByUserId($row['user_id']);
			$json['date'] = date('Y-m-d',strtotime($row['timestamp']));
			$json['userId'] = $row['user_id'];
			if(@$_SESSION['loginId'] != null && @$_SESSION['loginId']==$row['user_id']){
				$json['canEdit'] = true;
			}else{
				$json['canEdit'] = false;
			}
			
			
			break;
		}
		
		Common::echoJSON(true, $json);
	}
	
	public function reply(){
		$topicId = $_POST['topicId'];
		$includeReplyId = @$_POST['includeReplyId'] == null ? 0 : $_POST['includeReplyId'];
		$comment = $_POST['comment'];
		$uesrId = @$_SESSION['loginId'];
		
		//check ac api
		if(Common::checkLoginApi(@$_POST['ac'], @$_POST['pw']) == false)
			return;
			
		mysql_query("insert into forum_reply (topic_id, include_reply_id, comment, user_id) values ($topicId, $includeReplyId, '$comment', $uesrId)");
		
		// api
		if(Common::isApi()){
			$json['resultUrl'] = Common::$rootPathHttp . "forum/view/" . $topicId;
			Common::echoJSON(true, $json);
		}else{
			Common::echoJSON(true, null);
		}
	}
	
	public function viewAllReply(){
		$topicId = $_GET['topicId'];
		$sqlResult = mysql_query("select id, user_id, comment, timestamp from forum_reply where topic_id=$topicId order by timestamp desc");
		$json = "";
		while($row = mysql_fetch_assoc($sqlResult)){
			$tempObj = array();
			$tempObj['replyId'] = $row['id'];
			$tempObj['userId'] = $row['user_id'];
			$tempObj['comment'] = $row['comment'];
			$tempObj['nickName'] = Common::getNicknameByUserId($row['user_id']);
			$tempObj['date'] = date('Y-m-d',strtotime($row['timestamp']));
			if(@$_SESSION['loginId'] != null && $row['user_id'] == @$_SESSION['loginId']){
				$tempObj['selfReply'] = true;
			}else{
				$tempObj['selfReply'] = false;
			}
			$json['reply'][] = $tempObj;
		}
		
		
		Common::echoJSON(true, $json);
	}
	
	// Method: PUT
	public function edit(){
		$method = $_SERVER['REQUEST_METHOD'];
		parse_str(file_get_contents("php://input"),$args);

		$topicId = $args['topicId'];
		$newTopic = $args['topic'];
		$newContent = $args['content'];
		
		//check ac api
		if(Common::checkLoginApi(@$args['ac'], @$args['pw']) == false)
			return;
			
		// must login
		if(@$_SESSION['loginId'] == null){
			$json['reason'] = 'must-login';
			Common::echoJSON(false, $json);
		}
		
		// new Date
		date_default_timezone_set("Asia/Hong_Kong");
		$timestamp = date('Y-m-d H:i:s');
		
		$sqlResult = mysql_query("update forum set topic= '$newTopic', content='$newContent', timestamp='$timestamp' where id = '$topicId'");	

		Common::echoJSON(true, null);
	}
	
	public function viewReply(){
		$replyId = $_GET['replyId'];
		$json['comment'] = Common::getDBSingleValue('forum_reply','comment', "where id=$replyId");
		Common::echoJSON(true, $json);
	}
	
	// Method: PUT
	public function editReply()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		parse_str(file_get_contents("php://input"),$args);
		
		$replyId = $args['replyId'];
		$comment = $args['comment'];
		mysql_query("update forum_reply set comment='$comment' where id='$replyId'");	
		$json['topicId'] = Common::getDBSingleValue('forum_reply', 'topic_id', "where id=$replyId");
		Common::echoJSON(true, $json);
	}
	
	// Method: DELETE
	public function deleteTopic(){
		$method = $_SERVER['REQUEST_METHOD'];
		parse_str(file_get_contents("php://input"),$args);
		
		// must method DELETE
		if(Common::checkMethod('DELETE') == false) return;
		
		//must login
		if(@$_SESSION['loginId'] == null && @$_GET['api']==null){
			$json['reason'] = 'must-login';
			Common::echoJSON(false, $json);
			return;
		}
		
		//check ac api
		if(Common::checkLoginApi(@$args['ac'], @$args['pw']) == false)
			return;
		
		$topicId = $args['topicId'];
		if(Common::getDBSingleValue('forum', 'topic', "where id=$topicId") == null){
			$json['reason'] = 'topic-not-exist';
			Common::echoJSON(false, $json);
			return;
		}
		
		mysql_query("delete from forum where id=$topicId");
		mysql_query("delete from forum_reply where topic_id=$topicId");
		
		
		// api
		if(@$_GET['api'] == true){$json['api'] = true;
			Common::echoJSON(true, $json);
		}else{
			Common::echoJSON(true, null);	
		}
	}
	
	public function deleteReply(){
	$method = $_SERVER['REQUEST_METHOD'];
		parse_str(file_get_contents("php://input"),$args);
		
		// must method put
		if(Common::checkMethod('DELETE') == false) return;
		
		//check ac api
		if(Common::checkLoginApi(@$args['ac'], @$args['pw']) == false)
			return;
		
		$replyId = $args['replyId'];
		mysql_query("delete from forum_reply where id=$replyId");
		
		Common::echoJSON(true, null);
	}
}
?>