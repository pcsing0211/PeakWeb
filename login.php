<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
</head>

<body>
<div data-role="page" id="login">
<script>var pageName = "login";</script>
	<div data-role="header">
        <a id="backButton" href="#" data-transition="slideup" data-direction="reverse">Back</a>
        <h1>Login</h1>
	</div>
	<div data-role="content">
      <p>Email: 
	          <label for="login"></label>
	        <input type="text" name="email" id="email">
	        </p>
	        <p>Password: 
	          <label for="password"></label>
	          <input type="password" name="password" id="password">
	        </p>
	        <p id="loginResult" style="color:red"></p>
	        <p>
	          <input type="submit" name="button" id="submit" value="Submit"  data-transition="slide">
	        </p>
	</div>
<?php include("footer.php"); ?>
<script>
$("#login #backButton").attr("href", rootPath + "/");
</script>
<script>
	//$.mobile.showPageLoadingMsg();

	
$(document).ready(function(e) {
	// [!] Can't enter this page if already loginz
	if(localStorage.getItem("login") != null){
		alert("You already login");
		window.location.replace( rootPath + "/");
	}
    //alert("aaa");
	$("#login #submit").click(function(){
		//alert("The paragraph was clicked");
		login();
	});
	
	//$("#showEmail").html($.cookie("login").split(":")[0]);
	

});

function login(){
	// vars
	var ac=$("#login #email").val();
	var pw=$("#login #password").val();

	// check is valid email
	//alert("ac = " + ac);
	if(!isValidEmailAddress(ac)){
		$("#login #loginResult").html("Invalid email, Please check!");
		return;
	}
	
	var URLs="program/member/login";
	var myData = 'ac='+ ac + '&pw='+pw; //build a post data structure
	$.ajax({
		url: URLs,
		data: myData,
		type:"POST",
		dataType:'text',

		success: function(msg){
			//alert(msg);
			var obj = jQuery.parseJSON(msg);
			if(obj["result"] != true){
				$("#login #loginResult").html("Email or Password Incorrect");
				return;
			}else{
				localStorage.setItem("login", obj["login"]);
				//$.cookie("login",obj["login"]);
				window.location.replace(rootPath + "/membercenter");
				//$(location).attr('href', 'membercenter.html');
			}
		},

		 error:function(xhr, ajaxOptions, thrownError){
			alert(xhr.status);
			alert(thrownError);
		 }
	});
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    // alert( pattern.test(emailAddress) );
    return pattern.test(emailAddress);
};
</script>

</div>
</body>
</html>
